#!/bin/bash

CONFIGS="LDT LT DT L T"
SEEDS="0 100"
NODES=$1

mkdir results

for SEED in $SEEDS
do
	for CONF in $CONFIGS
	do
		mpirun -f $NODES src/CommissionDecommission ../config/config_$CONF.yml $SEED 2>&1 | tee results/$CONF-$SEED.log
	done
done
