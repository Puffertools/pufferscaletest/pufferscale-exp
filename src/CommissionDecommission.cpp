/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include <mpi.h>
#include <thallium.hpp>

#include "pufferscale/WorkerInfo.hpp"
#include "pufferscale/Master.hpp"
#include "pufferscale/Worker.hpp"
#include "pufferscale/Controller.hpp"
#include "pufferscale/BucketInfo.hpp"
#include "pufferscale/Callbacks.hpp"

#include <string>
#include <memory>
#include <unordered_map>
// Normal distrib
#include <chrono>
#include <random>


#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>

#include <cassert>

#include "yaml-cpp/yaml.h"

#include "SimpleWorker.hpp"

namespace tl = thallium;
namespace ps = pufferscale;


/// Important parameters 

// Constraint on the number of providers/workers in the cluster
int max_workers = 16;
int min_workers = 5;

/// Workers per MPI_worker node
int max_workers_mpi_rank = 5;
/// Max objects per MPI_worker node
int max_nb_objects_mpi_rank = 100000;

// Operations that are run
	// Number of operation_delta_nodess to run
int nb_sequences = 10;
	// Number of operations between two operation_delta_nodes operation
int nb_in_between_random_operations = 3;
	// Sequence operation - initial size
std::vector<int> operation_initial_size;
	// Sequence operation - nb of nodes to com/decom
std::vector<int> operation_delta_nodes;
	// Number of random operations to do before starting 
int warm_up_operations = 10;


/// Network bandwidth of the workers
double m_network_bandwidth = 1000;
/// Memory storage capacity of the workers 
double m_capacity = 10000000;

/// Number of objects to generate per node
int m_objects_per_node = 4;


int m_parallel_transfers = 10;
/// 

double gen_mean = 100;
double gen_std_dev = 40;

/// Weights
double weight_load = 1;
double weight_data = 1;
double weight_transfer = 1;

double weight_load2 = 1;
double weight_data2 = 1;
double weight_transfer2 = 1;

int seed = 0;
double total_load = 100;
double total_size = 4096;


/// NOT PARAMETERS 


// Counters
int current_nb_workers = max_workers;
int op_counter = 0;

/// MPI comm for controller and worker
MPI_Comm comm_workers_controller;
MPI_Comm comm_workers;

std::string m_addr;

/// Name of the provider
const std::string m_service_name = "SimpleWorker";

/// Metadata associated with each worker
struct p_metadata {
	int 							provider_id;
	std::unique_ptr<ps::Worker> 	worker;
	ps::ProviderInfo 				pi;
	std::unique_ptr<SimpleWorker>	sw;
	bool 							com 		=	false;
	int t_com = 0;
	int t_decom = -1;
};

int turn = 0;

/// Workers on the node
std::vector<p_metadata> m_workers;


std::shared_ptr<ps::Controller> controller;

std::vector<ps::WorkerInfo> m_commissioned_workers;
std::vector<ps::WorkerInfo> m_decommissioned_workers;

// RNG
std::default_random_engine generator;
std::normal_distribution<double> distribution;

// Max providers per worker
// List of the workers, with providers active/removed

//////////////////////////// Controller /////////////////////////////////////////////////////

/** 
 * Run one rescaling operation
 *
 * @param delta Number of nodes to add or remove
 * @param type Number of the operation in the sequence, -1 if random
 * @param last True if it is the last rescaling operation
 */
void rescaling(int delta, int type, bool last){
	int ZERO = 0;
	int ONE = 1;
	int nb_workers_before = current_nb_workers;

	current_nb_workers += delta;

	std::cout << op_counter << "," << type << "," << seed << "," << nb_workers_before << "," << delta << ",";
	
	// Record duration
	auto before = std::chrono::system_clock::now();

	if (delta > 0){
 		// Commission
		std::unordered_map<ps::WorkerInfo, 
			std::vector<std::string>, 
			ps::WorkerInfoHash> to_com;
		for (int i = 0; i < delta; ++i){
			int pos = rand()%m_decommissioned_workers.size();
			to_com[m_decommissioned_workers[pos]].push_back(m_service_name);
			m_commissioned_workers.push_back(m_decommissioned_workers[pos]);
			// std::cout << "Commissioning " << m_decommissioned_workers[pos] << std::endl;
			m_decommissioned_workers.erase(m_decommissioned_workers.begin()+pos);
		}
		
	
		bool result = controller->commission(to_com);
		(void)result;
		if (!result){
			std::cout << "Operation (" << type << "): " << current_nb_workers << " + " << delta << std::endl;
			std::cout << "Failed commission" << std::endl;
		}
		
		assert(result);
	} else if (delta < 0) {
		// Decommission
		std::vector<ps::WorkerInfo> to_decom;
		for (int i = 0; i < -delta; ++i){
			int pos = rand()%m_commissioned_workers.size();
			to_decom.push_back(m_commissioned_workers[pos]);
			m_commissioned_workers.erase(m_commissioned_workers.begin()+pos);
		}
	
		bool result = controller->decommission(to_decom);
		(void)result;
		if (!result){
			std::cout << "Operation (" << type << "): " << current_nb_workers << " + " << delta << std::endl;
			std::cout << "Failed decommission" << std::endl;
		}
		assert(result);

		m_decommissioned_workers.insert(m_decommissioned_workers.end(), 
				to_decom.begin(),
				to_decom.end());
	}

	auto after = std::chrono::system_clock::now();
	std::chrono::duration<double> diff = after-before;

	// Notify workers and master that the rescaling has terminated
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Collect stats
	
	double zero = 0;
	double max_load = 0;
	double max_size = 0;
	double max_recv = 0;
	double max_sent = 0;

	// Collect the maximum load, data, data recveived, and data sent per worker
	MPI_Reduce(&zero, &max_load, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
	MPI_Reduce(&zero, &max_size, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
	
	MPI_Reduce(&zero, &max_recv, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
	MPI_Reduce(&zero, &max_sent, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
	
	// Wait for the controller to finish the operation
	double sched_duration = -1;
	MPI_Recv(&sched_duration, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	// Record stats
	std::cout << max_load << "," << total_load << ",";
	std::cout << max_size << "," << total_size << ",";
	std::cout << max_sent << "," << max_recv << "," << sched_duration << ",";
	std::cout << diff.count() << std::endl;

	op_counter++;

	// Notify workers and master if it is the last rescaling or not
	if (!last) {
		MPI_Bcast(&ZERO,1,MPI_INT, 1, MPI_COMM_WORLD);
	} else {
		MPI_Bcast(&ONE,1,MPI_INT, 1, MPI_COMM_WORLD);
	}
}

/** 
 * Run a rescaling of a random number of nodes
 */
void random_rescaling(){
	int delta = (rand() % (max_workers - min_workers + 1)) + min_workers - current_nb_workers;
	rescaling(delta,-1,false);
}

// To run on controller
void run_operations(){
	srand(seed);

	int delta;

	// initialize vectors of workerinfo
    std::unordered_map<ps::WorkerInfo,std::vector<ps::ProviderInfo>,
		ps::WorkerInfoHash> all_buckets = controller->list_all_providers();
    std::unordered_map<ps::WorkerInfo,std::vector<ps::ProviderInfo>,
		ps::WorkerInfoHash>::iterator it; 

	for (it = all_buckets.begin(); it != all_buckets.end(); ++it){
		m_commissioned_workers.push_back(it->first);
	}

	// Initialize the number of workers as the number of workers known by pufferscale
	current_nb_workers = m_commissioned_workers.size();

	std::cout << "op,type,seed,current_nb_workers,delta,";
	std::cout << "max_load,total_load,max_size,total_size,";
	std::cout << "max_sent,max_recv,sched_duration,duration" << std::endl;

	// Warm up
	for (int i = 0; i < warm_up_operations - 1; ++i){
		random_rescaling();
		// Send signal to continue
	}

	// Rescaling to the proper number of nodes for the next rescaling operation
 	delta = operation_initial_size[0] - current_nb_workers;
	rescaling(delta,-1,false);
	
	for (int j = 0; j < nb_sequences ; ++j){
		for (unsigned int i = 0; i < operation_initial_size.size(); ++i){
			// Run the i-th rescaling operation	
			bool last = i == operation_initial_size.size() - 1 && j == nb_sequences - 1; 
			rescaling(operation_delta_nodes[i],i,last);
			if (last)
				break;

			for (int k = 0; k < nb_in_between_random_operations - 1; ++k){
				random_rescaling();
			}
		
			// Rescaling to the proper number of nodes for the next rescaling operation
 			delta = operation_initial_size[(i+1)%operation_initial_size.size()] - current_nb_workers;
			rescaling(delta,-1,false);
		}	
	}
}


void mpi_controller(){
	//Seperate the communicator between (workers,controller) and the master
	MPI_Comm_split(MPI_COMM_WORLD, 0, 1, &comm_workers_controller);
    // Communicator for all workers
	MPI_Comm_split(MPI_COMM_WORLD, 1, 1, &comm_workers);

	char master_addr[128];
    MPI_Bcast(master_addr,
			128,
			MPI_CHAR,
			0,
			MPI_COMM_WORLD);

	tl::engine myEngine("ofi+tcp", THALLIUM_CLIENT_MODE, false, 0);

	ps::MasterInfo masterInfo;
	masterInfo.m_address = std::string(master_addr);

	// Wait for the master to start
	MPI_Barrier(MPI_COMM_WORLD);

	// Wait for the workers to start
	MPI_Barrier(MPI_COMM_WORLD);

	controller = std::make_unique<ps::Controller>(myEngine, masterInfo);

    // Work
    run_operations();

	// Operations have been completed
	MPI_Barrier(MPI_COMM_WORLD);
	// Wait for workers to terminate
	MPI_Barrier(MPI_COMM_WORLD);
	// Controller and master finalize
	
    controller.reset(); // needed to release thallium buckets before calling myEngine.finalize()
	
	MPI_Finalize();
}

//////////////////////////////////////// Worker /////////////////////////////////////////

/**
 * Function to update the metadata of an object
 *
 * \param rt BucketTag of the data for which the information is collected
 * \param uargs Pointer to the p_metadata of the host
 * \return Pufferscale::BucketMetadata of the object
 */
ps::BucketMetadata update_meta(const ps::BucketTag& rt, void* uargs){
	int id = rt.m_bucket_id;
	p_metadata* pm = (p_metadata*)(uargs);
	if (!pm->com)
		std::cout << "Update metadata on not-commissioned node" << std::endl;
	
	assert(pm->com);
	ps::BucketMetadata rm;
	rm.m_bucket_data_in_memory = pm->sw->get_object_size(id);
	rm.m_bucket_load = pm->sw->get_object_load(id);
	return rm;
}

/**
 * Function to migrate objects from one worker to another
 *
 * \param ops Migration operation that specify which object, which source,
 * which destination.
 * \param uargs Pointer to p_metadata of the host
 * \return True if the migration succeded 
 */
bool migration(const ps::MigrationOperation ops, void* uargs){
	p_metadata* pm = (p_metadata*)(uargs);
	if (!pm->com)
		std::cout << "Migrate on not-commissioned node" << std::endl;
	assert(pm->com);
	bool res =pm->sw->migrate(	ops.m_bucket.m_bucket_id,
							ops.m_destination.m_provider_address,
							ops.m_destination.m_provider_id);
	
	if (!res){
		std::cout << "Failed migration from " << ops.m_source << " to " << ops.m_destination << std::endl;
	}
	return res;
}

/**
 * Function to terminate a provider on a Worker
 *
 * NOTE: As there can only be 1 provider per worker in this experiement,
 * the provider is simply marked as deactivated.
 * 
 * \param pi ProviderInfo about the provider to stop
 * \param uargs Unused
 * \param cargs Pointer to p_metadata of the worker
 * \return true
 */
bool terminate_provider(const ps::ProviderInfo& pi, void* uargs, void* cargs){
	(void) pi;
	(void) uargs;
	
	p_metadata* pm = (p_metadata*)(cargs);

	if (!pm->com)
		std::cout << "Terminate not-commissioned node" << std::endl;
	
	assert(pm->com);	
	if (pm->sw->get_load() != 0){
		std::cout << pm->provider_id << " has a load of " << pm->sw->get_load() << std::endl;
		assert(pm->sw->get_load() == 0);
	}
	assert(pm->sw->get_data() == 0);

	pm->com = false;
	pm->t_decom = turn;

	return true;
}

/**
 * Function to initiate a new provider on a worker.
 * 
 * NOTE: Here we limitate the number of providers per node
 * to 1. Thus, the provider is already initialized at startup, and
 * is simply used again.
 *
 * \param cargs Pointeur to the p_metadata of the worker.
 * \return Pufferscale::ProviderInitializationData as specified by Pufferscale
 */
ps::ProviderInitializationData initiate_provider(void* cargs){
	// cargs = p_metadata
	p_metadata* pm = (p_metadata*)(cargs);
	if (pm->com){
		std::cout << "Initiate commissioned node: " << pm->pi  << std::endl;
		std::cout << "Init on turn: " << pm->t_com << ", decom on turn: " << pm->t_decom << std::endl;
	}
	assert(!pm->com);
	pm->com = true;
	ps::ProviderInitializationData pd;
	pd.m_provider_info = pm->pi;
	pm->t_com = turn;
	return pd;
}

/**
 * Function that manages the reception of data, called from the simple worker.
 *
 * \param id Id of the transfered object
 * \param load Load induced by the object
 * \param size Size of the object
 * \param uargs Pointer to the p_metadata of the host
 */
bool on_reception(int id, double load, double size, p_metadata* pm){
	(void)load;
	(void)size;
	ps::BucketTag rt;
	rt.m_service_name = m_service_name;
	rt.m_bucket_id = id;
	if (!pm->com)
		std::cout << "Receive not-commissioned node" << std::endl;
	assert(pm->com);
	bool truc = pm->worker->manages(pm->pi,
						rt,
						&migration,
						&update_meta,
						pm);	
	if (!truc){
		if (pm->com){
			std::cout << "ADD failed even though the node is not decommissioned" << std::endl;
			std::cout << pm->pi << std::endl; 
		} else {
			std::cout << "ADD failed" << std::endl;
		}
	}

	return truc;
	//assert (truc);
}


/**
 * Initialize the workers on the node
 *
 * \param engine Thallium engine 
 * \param masterInfo Informations about the master address
 */
void init_workers(tl::engine& engine, const ps::MasterInfo& masterInfo){	
	//unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
	generator = std::default_random_engine(seed+rank);

	distribution = std::normal_distribution<double>(gen_mean,gen_std_dev);

	auto mtx = std::make_shared<tl::mutex>();
	
	for (int i = 0; i < max_workers_mpi_rank; ++i){
		m_workers[i].provider_id 	= i;
		m_workers[i].worker 		= std::make_unique<ps::Worker>(engine,i);
		m_workers[i].pi.m_provider_id 		= i;
		m_workers[i].pi.m_service_name 	= m_service_name;
		m_workers[i].pi.m_provider_address 	= engine.self();
		m_workers[i].sw 			= std::make_unique<SimpleWorker>(engine,i,mtx);

		m_workers[i].sw->set_callback([i](int id, double load, double size) {
                    auto pm = &m_workers[i];
                    return on_reception(id, load, size, pm);
                });

		m_workers[i].worker->configure_worker(m_capacity,m_network_bandwidth);


		m_workers[i].worker->register_provider(m_service_name,
				(void*)(m_workers.data()+i),
				initiate_provider,
				terminate_provider);
		m_workers[i].worker->manages_provider(m_workers[i].pi);

		m_workers[i].com = true;

		if (!m_workers[i].worker->join(masterInfo)){
			std::cout << "Worker " << i << " failed to join" << std::endl;
		}
	}
}

double gen_nb(){
	double nb = distribution(generator);
	if (nb < 1)
		return 1;
	return nb;
}

/**
 * Generate the data of all SimpleWorkers on the node
 *
 * /param bucket_id_offset Offset for the ids of the object stored
 */
void generate_data(int bucket_id_offset){
	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

	int id = bucket_id_offset;
	
	std::vector<double> loads;
	double rank_total_load = 0;
	double cluster_total_load = 0;
	std::vector<double> sizes;
	double rank_total_size = 0;
	double cluster_total_size = 0;

	// Compute the number of buckets to generate
	int total_bckts = (max_workers - (rank-2)*max_workers_mpi_rank) * m_objects_per_node;
	if (total_bckts > max_workers_mpi_rank * m_objects_per_node){
		total_bckts = max_workers_mpi_rank * m_objects_per_node;
	}

	// Generate all values for load and size
	if (total_bckts > 0){
		loads.resize(total_bckts);
		sizes.resize(total_bckts);

		for (int i = 0; i < total_bckts; ++i){
			double load = gen_nb();
			loads[i] = load;
			rank_total_load += load;

			double size = gen_nb();
			sizes[i] = size;
			rank_total_size += size;
		}
	}

	// Compute the total amount of data and load generate to scale it to a total of 
	// total_load and total_size
	MPI_Allreduce(&rank_total_load,&cluster_total_load,1,MPI_DOUBLE, MPI_SUM, comm_workers);
	MPI_Allreduce(&rank_total_size,&cluster_total_size,1,MPI_DOUBLE, MPI_SUM, comm_workers);
	
	if (total_bckts <=0)
		return ;
	
	double coeff_load = total_load / cluster_total_load;
	double coeff_size = total_size / cluster_total_size;

	int index = 0;
	
	for (int i = 0; i < max_workers_mpi_rank; ++i){
		p_metadata* pm 	= m_workers.data()+i;
		void* uargs 	= (void*) pm;
		if ((rank-2) * max_workers_mpi_rank + i< max_workers){
			for (int j = 0; j < m_objects_per_node; ++j){
				ps::BucketTag rt;
				rt.m_service_name = m_service_name;
				rt.m_bucket_id = id;
				pm->sw->add_object(id,coeff_size*sizes[index],coeff_load*loads[index]);
				pm->worker->manages(pm->pi,
						rt,
						&migration,
						&update_meta,
						uargs);
				id++;
				index++;
			}
		}
	}
}


void mpi_worker(){
	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    // Create a comm with workers and controller
    MPI_Comm_split(MPI_COMM_WORLD, 0, rank, &comm_workers_controller);
    // Create a comm with only workers
	MPI_Comm_split(MPI_COMM_WORLD, 0, rank, &comm_workers);
    
    char master_addr[128];
    MPI_Bcast(master_addr,
			128,
			MPI_CHAR,
			0,
			MPI_COMM_WORLD);
	
	// Wait for the master to start
	MPI_Barrier(MPI_COMM_WORLD);
	ps::MasterInfo masterInfo;
	masterInfo.m_address = std::string(master_addr);
    
	tl::engine myEngine("ofi+tcp", THALLIUM_SERVER_MODE,true,6);
	
	// Initialize the workers	
	init_workers(myEngine,masterInfo);
	
	// Generate data
	generate_data(rank*max_nb_objects_mpi_rank);

	// Signal workers have been started
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Work
    int done = 0;
    while (done == 0){
		// Wait for the controller to finish the operation
		MPI_Barrier(MPI_COMM_WORLD);
		// Compute max load, max size
		double max_load = 0;
		double max_size = 0;
		
		double max_sent = 0;
		double max_recv = 0;

		for (int i = 0; i < max_workers_mpi_rank; ++i){
			double load = m_workers[i].sw->get_load();
			double size = m_workers[i].sw->get_data();
			double sent = m_workers[i].sw->get_sent();
			double recv = m_workers[i].sw->get_received();
			max_load = (load>max_load)?load:max_load;
			max_size = (size>max_size)?size:max_size;
			max_sent = (sent>max_sent)?sent:max_sent;
			max_recv = (recv>max_recv)?recv:max_recv;
			m_workers[i].sw->reset_network_counters();
		}
		// Send stats (max load, max size, max recv, max sent)
		MPI_Reduce(&max_load, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
		MPI_Reduce(&max_size, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
		
		MPI_Reduce(&max_recv, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
		MPI_Reduce(&max_sent, nullptr, 1, MPI_DOUBLE, MPI_MAX, 0, comm_workers_controller);
		
		// Check if it is the last operation
		// Continue if done == 1, stop in any other case
		MPI_Bcast(&done,1,MPI_INT, 1, MPI_COMM_WORLD);
    
		turn++;
	}

	MPI_Barrier(MPI_COMM_WORLD);

	// Stopping workers
	myEngine.finalize();

	// Wait for master and controller to terminate
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Finalize();
}

///////////////////////////////////////////////////////// MASTER ////////////////////////

void mpi_master(){
	// Create a comm with workers and controller
	MPI_Comm_split(MPI_COMM_WORLD, 1, 0, &comm_workers_controller);
    // Create a comm of workers
	MPI_Comm_split(MPI_COMM_WORLD, 1, 0, &comm_workers);

	// Start the master
    tl::engine myEngine("ofi+tcp", THALLIUM_SERVER_MODE, true, 1);
    	
	char master_addr[128];
	std::string master_addr_str = myEngine.self();
   	strcpy(master_addr,master_addr_str.c_str());
	// Broadcast the address of the master
	MPI_Bcast(master_addr,
				128,
				MPI_CHAR,
				0,
				MPI_COMM_WORLD);
	ps::Master master(myEngine);

	bool result = master.configure_provider(
			m_service_name, weight_load,weight_data,weight_transfer,false,true,m_parallel_transfers);
	(void) result;
	assert(result);

	// Signal that the master has started
	MPI_Barrier(MPI_COMM_WORLD);
	// Wait for the workers to start
	MPI_Barrier(MPI_COMM_WORLD);

    int done = 0;
    while (done == 0){
		// Wait for a rescaling operation to terminate		
		MPI_Barrier(MPI_COMM_WORLD);
	
		op_counter++;
		// After the warmup, change the loads of the rebalancing algorithm
		if (op_counter == warm_up_operations){ 
			bool result = master.configure_provider(
				m_service_name, weight_load2,weight_data2,weight_transfer2,false,true,m_parallel_transfers);
			(void) result;
			assert(result);
		}
		
		// Collect the duration of the rebalancing
		double duration = master.get_duration_scheduling();
		// Send it to the controller
		MPI_Send(&duration, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);

		// Check whether it is the last rescaling
		MPI_Bcast(&done,1,MPI_INT, 1, MPI_COMM_WORLD);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	//Wait for the workers to stop
	MPI_Barrier(MPI_COMM_WORLD);

	myEngine.finalize();
		
	MPI_Finalize();
}

//////////////////////////////////////////////////////////// CONFIRGURATION ///////////////


int read_int(YAML::Node& config, const std::string& key, int default_value){
	if (!config[key].IsScalar()){
		std::cout << "Key " << key << " undefined, returning default value." << std::endl;
		return default_value;
	}
	return config[key].as<int>();
}

double read_double(YAML::Node& config, const std::string& key, double default_value){
	if (!config[key].IsScalar()){
		std::cout << "Key " << key << " undefined, returning default value." << std::endl;
		return default_value;
	}
	return config[key].as<double>();
}

void init_config(char* file){
	YAML::Node config;
	try {
		config = YAML::LoadFile(file);
	} catch (YAML::BadFile& e) {
		std::cerr << "Couldnot read config file" << std::endl;
		exit(-2);
	}
	
	if (!config.IsMap()){
		std::cerr << "Invalid config file" << std::endl;
		exit(-3);	
	}

	max_workers = read_int(config,"max_workers",20);
	min_workers = read_int(config,"min_workers",5);
	max_workers_mpi_rank = read_int(config,"max_workers_mpi_rank", 5);
	nb_sequences = read_int(config,"nb_sequences", 10);
	nb_in_between_random_operations = read_int(config,"nb_in_between_random_operations", 5);

	if (!config["operation_initial_size"].IsDefined()){
		std::cerr << "Problem with operation_delta_nodes definition." << std::endl;
		exit(-4);
	}
	if (!config["operation_delta_nodes"].IsDefined()){
		std::cerr << "Problem with operation_delta_nodes definition." << std::endl;
		exit(-4);
	}
	
	if (!config["operation_initial_size"].IsSequence()){
		std::cerr << "Problem with operation_delta_nodes definition (not a operation_delta_nodes)."<< std::endl;
		exit(-4);
	}
	if (!config["operation_delta_nodes"].IsSequence()){
		std::cerr << "Problem with operation_delta_nodes definition (not a operation_delta_nodes)."<< std::endl;
		exit(-4);
	}
	
	for (unsigned int i = 0; i < config["operation_initial_size"].size(); ++i){
		operation_initial_size.push_back(config["operation_initial_size"][i].as<int>());
	}	
	for (unsigned int i = 0; i < config["operation_delta_nodes"].size(); ++i){
		operation_delta_nodes.push_back(config["operation_delta_nodes"][i].as<int>());
	}

	if (operation_delta_nodes.size() != operation_initial_size.size()){
		std::cerr << "Problem with operation_delta_nodes definition (size)." << std::endl;
		exit(-4);
	}

	m_objects_per_node = read_int(config,"m_objects_per_node",4);
	m_parallel_transfers = read_int(config,"m_parallel_transfers", 10);

	warm_up_operations = read_int(config,"warm_up_operations", 25);

	// Weights
	weight_load = read_double(config,"weight_load",1);
	weight_data = read_double(config,"weight_data",1);
	weight_transfer = read_double(config,"weight_transfer",1);
	weight_load2 = read_double(config,"weight_load2", weight_load);
	weight_data2 = read_double(config,"weight_data2", weight_data);
	weight_transfer2 = read_double(config,"weight_transfer2", weight_transfer);

	// Total data / load
	total_load = read_double(config,"total_load",100);
	total_size = read_double(config,"total_size",4096);

	//seed
	seed = read_int(config,"seed", 0);

	//Generation
	gen_mean = read_double(config,"gen_mean", 100);
	gen_std_dev = read_double(config,"gen_std_dev", 40);


	// Compute computable parameters
	m_workers.resize(max_workers_mpi_rank);
	max_nb_objects_mpi_rank = m_objects_per_node*max_workers_mpi_rank;
}


int main(int argc, char** argv){
	if (argc < 2 || argc > 3){
		std::cout << "usage: " << argv[0] << " <config file> <seed>" << std::endl;
		return -1;
	}

	init_config(argv[1]);

	if (argc == 3){
		seed = std::stoi(argv[2]);
	}

	MPI_Init(&argc,&argv);

	int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
   
	if (rank == 0){
		int wsize;
		MPI_Comm_size(MPI_COMM_WORLD,&wsize);

		if ((wsize-2)*max_workers_mpi_rank < max_workers){
			std::cout << "Not enough MPI ranks to support the maximum number of workers" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-6);
		}
		if (max_workers <= min_workers){
			std::cout << "Check max_workers and min_workers" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-7);
		}
		if (min_workers <= 0){
			std::cout << "Check min_workers" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-8);
		}
		if (nb_sequences <= 0){
			std::cout << "Check nb_sequences" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-9);
		}
		if (nb_in_between_random_operations <= 0){
			std::cout << "Check nb_in_between_random_operations" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-10);
		}
		if (warm_up_operations <= 0){
			std::cout << "Check warm_up_operations" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-11);
		}
		for (unsigned int i = 0; i < operation_initial_size.size(); ++i){
			if (operation_initial_size[i] < min_workers
					|| operation_initial_size[i] > max_workers
					|| operation_initial_size[i] + operation_delta_nodes[i] < min_workers
					|| operation_initial_size[i] + operation_delta_nodes[i] > max_workers){
				std::cout << "Check operation " << i << std::endl;
				MPI_Abort(MPI_COMM_WORLD,-12);
			}
		}
		if (m_objects_per_node <= 0){
			std::cout << "Check m_objects_per_node" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-13);
		}
		if (total_load <= 0){
			std::cout << "Check total_load" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-14);
		}
		if (total_size <= 0){
			std::cout << "Check total_size" << std::endl;
			MPI_Abort(MPI_COMM_WORLD,-15);
		}
		
		MPI_Barrier(MPI_COMM_WORLD);
		mpi_master();
	} else if (rank == 1){
		MPI_Barrier(MPI_COMM_WORLD);
		mpi_controller();
	} else {
		MPI_Barrier(MPI_COMM_WORLD);
		mpi_worker();
	}
	return 0;
}

