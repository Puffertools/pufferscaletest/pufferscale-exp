/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#include "SimpleWorker.hpp"
#include <mutex>
#include <cassert>


SimpleWorker::SimpleWorker(tl::engine& engine, uint16_t provider_id,
		const std::shared_ptr<tl::mutex>& lookup_mtx):
	tl::provider<SimpleWorker>(engine,provider_id),
	m_lookup_mtx(lookup_mtx),
    m_handle_mtx(std::make_shared<tl::mutex>()),
    m_mtx(std::make_shared<tl::mutex>()),
    m_remote_migrate(define("simpleworker_migrate", &SimpleWorker::on_migrate)) {

	engine.push_finalize_callback([this]() { this->on_finalize(); });
}

SimpleWorker::~SimpleWorker() {
}

void SimpleWorker::on_finalize() {
	// Clean-up
    {
        std::lock_guard<tl::mutex> guard(*m_handle_mtx);
	    m_handles.clear();
    }
    m_mtx.reset();
    m_handle_mtx.reset();
    m_lookup_mtx.reset();
}

void SimpleWorker::reset_network_counters(){
    std::lock_guard<tl::mutex> guard(*m_mtx);
	m_sent = 0;
	m_received = 0;
}

double SimpleWorker::get_sent() const {
	return m_sent;
}

double SimpleWorker::get_received() const {
	return m_received;
}

double SimpleWorker::get_data() const {
	double data = 0;
    for(const auto& object : m_stored_objects) {
		data += object.second.size;
	}
	return data;
}

double SimpleWorker::get_load() const {
	double load = 0;
    for(const auto& object : m_stored_objects) {
		load += object.second.load;
	}
	return load;
}

bool SimpleWorker::on_migrate(int id, double load, double size){

    {    
        std::lock_guard<tl::mutex> guard(*m_mtx);
	    if (m_stored_objects.count(id) != 0){
		    // TODO Fail
		    std::cout << " - Failed Receiving object " << id << " on " << get_provider_id() << std::endl;
		    assert(false);
		    return false;
	    }

    	m_received += size;

    	m_stored_objects[id].id 	= id;
    	m_stored_objects[id].size = size;
    	m_stored_objects[id].load = load;
    }
	
	bool truc = on_object_reception(id,load,size);
	
	return truc;
}

bool SimpleWorker::migrate(int object_id, const std::string& dst_addr, int dst_provider_id){

    double load, size;    
    {
        std::lock_guard<tl::mutex> guard(*m_mtx);
        if (m_stored_objects.count(object_id) == 0){
		// TODO Fail
            std::cout << " - Failed Migrating object " << object_id << " from " << get_provider_id() << std::endl;
            assert(false);
            return false;
        }

        object_data& o_data = m_stored_objects[object_id];
        load = o_data.load;
        size = o_data.size;
    }

	std::shared_ptr<tl::provider_handle> h;
	Key key;
	key.m_addr = dst_addr;
	key.m_provider_id = dst_provider_id;

    {
        std::lock_guard<tl::mutex> guard(*m_handle_mtx);
    	if (m_handles.count(key) == 0){
            std::lock_guard<tl::mutex> guard2(*m_lookup_mtx);
	    	auto dstEndpoint = get_engine().lookup(dst_addr);
		    h = std::make_shared<tl::provider_handle>(dstEndpoint, dst_provider_id);
		    m_handles[key] = h;
	    } else {
		    h = m_handles[key];
	    }
    }

	bool transfer = m_remote_migrate.on(*h)(object_id, load, size);

	(void)transfer;
	if (!transfer){
		std::cout << "Failed the transfer of " << object_id << " to " << dst_addr << ", " << dst_provider_id << std::endl;
		//std::cout << "Endpoint " << dstEndpoint << std::endl;
		return false;
	}
	assert(transfer);

    {
        std::lock_guard<tl::mutex> guard(*m_mtx);
	    m_sent += size;

	    m_stored_objects.erase(object_id);
    }
//	std::cout << " - Migrating object " << object_id << ": finished" << std::endl;


	return true;
}

bool SimpleWorker::add_object(int id, double size, double load){
    std::lock_guard<tl::mutex> guard(*m_mtx);
	if (m_stored_objects.count(id) == 1){
		// TODO Fail
		std::cout << "Failing add " << id << std::endl;
		assert(false);
		return false;
	}

	object_data& o_data = m_stored_objects[id];
	o_data.id 	= id;
	o_data.load = load;
	o_data.size = size;

	return true;
}

double SimpleWorker::get_object_size(int id) const {
    std::lock_guard<tl::mutex> guard(*m_mtx);
    auto it = m_stored_objects.find(id);
	if (it == m_stored_objects.end()){
		// TODO Fail
		std::cout << "Failing get_object_size " << id << " on provider " << get_provider_id() << std::endl;
		assert(false);
		return -1;
	}
	double size = it->second.size;
	return size;
}

double SimpleWorker::get_object_load(int id) const {
    std::lock_guard<tl::mutex> guard(*m_mtx);
    auto it = m_stored_objects.find(id);
	if (it == m_stored_objects.end()){
		// TODO Fail
		std::cout << "Failing get_object_load " << id << std::endl;
		assert(false);
		return -1;
	}
	double load = it->second.load;
	return load;
}

