/********************************************************
 * This is licensed under MIT License, see file LICENSE
 ********************************************************/
#ifndef SIMPLEWORKER_HPP
#define SIMPLEWORKER_HPP

#include <thallium.hpp>

#include <map>
#include <functional>
#include <string>
#include <memory>


#include <thallium/serialization/stl/string.hpp>

namespace tl = thallium;

class SimpleWorker : public tl::provider<SimpleWorker> {
private:

	/// Struct to find back an handle
	struct Key{
		std::string m_addr;
		int m_provider_id;
		
		bool operator  == (const Key& k) const{
			return m_provider_id == k.m_provider_id
				&& m_addr == k.m_addr;
		}
	};

	/// Hash function for the key
	struct KeyHash {
		std::hash<int>  m_h1;
		std::hash<std::string>  m_h2;

		std::size_t operator()(const Key& k) const {
			return m_h2(k.m_addr) ^ m_h1(k.m_provider_id);
		}
	};

	/// Information about a bucket
	struct object_data {
		int id;
		double size;
		double load;
	};

	/// Mutex to protect the lookup function
	std::shared_ptr<tl::mutex> m_lookup_mtx;

	/// mutex for the handle map
	std::shared_ptr<tl::mutex> m_handle_mtx;
	/// Map of handles for each destination
	std::unordered_map<Key, std::shared_ptr<tl::provider_handle>, KeyHash> m_handles;

	/// Mutex to protect the m_stored_objects
	mutable std::shared_ptr<tl::mutex> m_mtx;

	/// Remote procedure
	tl::remote_procedure m_remote_migrate;


	/// Amount of data sent by the node since the last reset
	double m_sent = 0;
	/// Amount of data received by the node since the last reset
	double m_received = 0;
	
	/// Map of stored objects
	std::map<int,object_data> m_stored_objects;

	/// Callback to call after receiving an object
	// @param id of the bucket 
	// @param load of the bucket 
	// @param size of the bucket
	// @param uargs (see m_uargs)
	// @return true on success
	std::function<bool(int,double,double)> on_object_reception;
	
	/// Uargs for the callback on_object_reception
	void* m_uargs = nullptr;

	/**
	 * RPC called on reception of "simpleworker_migrate"
	 *
	 * @param id ID of the bucket
	 * @param load Load induced by the bucket
	 * @param size Size of the bucket
	 * @return true in case of success
	 */
	bool on_migrate(int id, double load, double size);

	/**
	 * Finalize the worker
	 */
	void on_finalize();

public:
	/**
	 * Constructor
	 *
	 * @param engine Thallium engine
	 * @param provider_id ProviderID of the SimpleWorker
	 * @param lookup_mtx Mutex to protect the lookup function
	 */
	SimpleWorker(tl::engine& engine, 
            uint16_t provider_id,
            const std::shared_ptr<tl::mutex>& lookup_mtx);

    ~SimpleWorker();

    /**
	 * Resets the counters for the network data exchanged (sent,recv)
	 */
	void reset_network_counters();
	
	/**
	 * Get the amount of data sent since last reset
	 *
	 * @return counter sent
	 */
	double get_sent() const;

	/**
	 * Get the amount of data received since the last reset
	 *
	 * @return counter recv
	 */
	double get_received() const;

	/** 
	 * Return the amount of data stored by buckets on the SimpleWorker
	 *
	 * @return amount of data stored
	 */
	double get_data() const;

	/** 
	 * Return the load induced by the SimpleWorker
	 *
	 * @return load of the SimpleWorker
	 */
	double get_load() const;

	/**
	 * Migrate a bucket from this SimpleWorker to another
	 *
	 * @param object_id ID of the bucket
	 * @param dst_addr Address of the destination
	 * @param dst_provider_id ProviderID of the destination
	 *
	 * @return true on success
	 */
	bool migrate(int object_id,
            const std::string& dst_addr,
            int dst_provider_id);

	/**
	 * Add a bucket to the SimpleWorker
	 *
	 * @param id ID of the bucket
	 * @param size Size of the bucket
	 * @param load Load induced by the bucket
	 *
	 * @return true in case of success
	 */
	bool add_object(int id, double size, double load);

	/**
	 * Return the size of a bucket
	 *
	 * @param id ID of the bucket
	 * @return the size of a bucket
	 */
	double get_object_size(int id) const;

	/**
	 * Return the load of a bucket
	 *
	 * @param id ID of the bucket
	 * @return the load of a bucket
	 */
	double get_object_load(int id) const;

	/**
	 * Set the callback to call upon reception of a bucket
	 *
	 * @param cb Callback 
	 * @param uargs Additional args for the callback
	 */
    template<typename F>
	void set_callback(F&& cb) {
       on_object_reception = std::forward<F>(cb); 
    }
};




#endif
